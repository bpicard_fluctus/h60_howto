#! /usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'B. Picard <bpicard@satobsfluctus.eu>'


# ================================================
#
# REFERENCES
#
# ================================================

# HSAF
# https://hsaf.meteoam.it/

# PRECIPITATION
# https://hsaf.meteoam.it/Products/ProductsList?type=precipitation

# H60
# https://hsaf.meteoam.it/Products/Detail?prod=H60B

# CHARACTERISTICS
# src: https://hsaf.meteoam.it/CaseStudy/GetDocumentUserDocument?fileName=saf_hsaf_atbd_60-63_1_0.pdf&tipo=ATBD
#
# The SEVIRI images IFOV is 4.8 km at nadir, and it degrades moving away
# from nadir, becoming about 8 km over Europe. The sampling distance at the sub-satellite point is ~ 3 km. 
# Conclusion:
#    resolution Δx ~ from 4.8 to 8 km - sampling distance: ~ 3 km at the sub-satellite point.
#
# observing cycle Δt = 15 min - sampling time: 15 min
# The spatial coverage of the product is 60°S – 75°N, 80°W – 80°E.

# LONGITUDE/LATITUDE
# src https://hsaf.meteoam.it/CaseStudy/GetDocumentUserDocument?fileName=PUM_H60-63_V1.1.pdf&tipo=PUM
# 4.5 Latitude and Longitude generation
# ftp://ftphsaf.meteoam.it/products/matlab_code/

# ================================================
#
# PACKAGES
#
# ================================================
import os,sys

import gzip
import xarray as xr

import numpy as np

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import get_cmap

from cartopy import crs as ccrs
import cartopy.feature as cfeature


# ============================================================
#
# PARAMETERS 
#

# min threshold for rain rate [mm/hr]
RR_MIN = 0.5

NLEVELS = 20

EXTENT = None

VMIN,VMAX = 0,20

# ~ PROJ       = ccrs.Robinson(central_longitude=0)
globe   = ccrs.Globe()
PROJ    = ccrs.Geostationary(central_longitude=0, 
                         satellite_height=35785831, globe=globe)

if EXTENT is None:     RESOLUTION = '110m'
if EXTENT is not None: RESOLUTION = '10m'

if EXTENT is None: MARKER_SIZE = 10

# get cmap and defined nblevels
CMAP   = matplotlib.cm.get_cmap('BuPu', NLEVELS)

# ======================================
#
# LOAD lon/lat
#
fname = 'lat_lon_0.nc.gz'
print('reading %s ...'%fname)
try:
    hdle = gzip.open(fname, 'rb')
    dslatlon = xr.open_dataset(hdle,engine='h5netcdf')   
except:
    print('\n\nERROR: cannot read file %s\n'%fname)
    sys.exit(-1)
print('... done')
        
# ======================================
#
# LOAD H60
#
fname = 'h60_20230129_0500_fdk.nc.gz'
print('reading %s ...'%fname)
try:
    hdle = gzip.open(fname, 'rb')
    ds = xr.open_dataset(hdle,engine='h5netcdf')   
except:
    print('\n\nERROR: cannot read file %s\n'%fname)
    sys.exit(-1)    
print('... done')

# ======================================
#
# APPLY FILTER
#
ds       = ds.where(ds.rr >= RR_MIN)

# ======================================
#
# INIT FIGURE
#
fig = plt.figure(figsize=(20,12))   
ax = fig.add_subplot(1,1,1,projection=PROJ)
# ocean
feature = cfeature.NaturalEarthFeature(category='physical', name='ocean', scale=RESOLUTION,
                                edgecolor='none',   
                                facecolor=cfeature.COLORS['water'],zorder=10)    
ax.add_feature(feature)
# land
feature = cfeature.NaturalEarthFeature(category='physical', name='land', scale=RESOLUTION,
                                edgecolor='none',   
                                facecolor=cfeature.COLORS['land'],zorder=20)    
ax.add_feature(feature)
# coastlines
ax.coastlines(resolution=RESOLUTION,zorder=30)
# set extent
if EXTENT is not None: ax.set_extent(EXTENT, ccrs.PlateCarree())
# grid lines
ax.gridlines(draw_labels=True, dms=True, x_inline=False, y_inline=False,zorder=30)


# ======================================
#
# MAP H60
#
map_rr = ax.contourf(dslatlon.long,dslatlon.latg,ds.rr,
                        transform=ccrs.PlateCarree(),
                        vmin=VMIN,vmax=VMAX,cmap=CMAP,zorder=100)


# ======================================
#
# POST MAP
#

# colorbar
label = 'Rain Rate [mm.hr$^{-1}$]'
plt.colorbar(map_rr, ax=ax, shrink=.8, pad=.05, orientation='vertical',label=label,alpha=1)


# title
ax.set_title(fname)
figname = os.path.splitext(os.path.basename(fname))[0]+'_contourf.png'
print(figname)
plt.savefig(figname,bbox_inches='tight')

