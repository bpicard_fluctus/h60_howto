#! /usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'B. Picard <bpicard@satobsfluctus.eu>'



# ================================================
#
# REFERENCES
#
# ================================================

# HSAF
# https://hsaf.meteoam.it/

# PRECIPITATION
# https://hsaf.meteoam.it/Products/ProductsList?type=precipitation

# H60
# https://hsaf.meteoam.it/Products/Detail?prod=H60B

# CHARACTERISTICS
# src: https://hsaf.meteoam.it/CaseStudy/GetDocumentUserDocument?fileName=saf_hsaf_atbd_60-63_1_0.pdf&tipo=ATBD
#
# The SEVIRI images IFOV is 4.8 km at nadir, and it degrades moving away
# from nadir, becoming about 8 km over Europe. The sampling distance at the sub-satellite point is ~ 3 km. 
# Conclusion:
#    resolution Δx ~ from 4.8 to 8 km - sampling distance: ~ 3 km at the sub-satellite point.
#
# observing cycle Δt = 15 min - sampling time: 15 min
# The spatial coverage of the product is 60°S – 75°N, 80°W – 80°E.

# LONGITUDE/LATITUDE
# src https://hsaf.meteoam.it/CaseStudy/GetDocumentUserDocument?fileName=PUM_H60-63_V1.1.pdf&tipo=PUM
# 4.5 Latitude and Longitude generation
# ftp://ftphsaf.meteoam.it/products/matlab_code/

# ================================================
#
# PACKAGES
#
# ================================================
import os,sys

import gzip

import numpy as np
import xarray as xr

from satpy.readers._geos_area import get_area_extent
from satpy.resample import get_area_def
from pyresample.geometry import AreaDefinition

from pyproj import Proj, CRS, Transformer

import matplotlib
import matplotlib.pyplot as plt
from matplotlib.pyplot import get_cmap

from cartopy import crs as ccrs
import cartopy.feature as cfeature

# ================================================
#
# internal function
#
# ================================================
    


# ============================================================
#
# PARAMETERS 
#

# min threshold for rain rate [mm/hr]
RR_MIN = 0.5

NLEVELS = 40

EXTENT = None

VMIN,VMAX = 0,40

# ~ PROJ    = ccrs.Robinson(central_longitude=0)
globe   = ccrs.Globe()
PROJ    = ccrs.Geostationary(central_longitude=0, 
                         satellite_height=35785831, globe=globe)



if EXTENT is None:     RESOLUTION = '110m'
if EXTENT is not None: RESOLUTION = '10m'

if EXTENT is None: MARKER_SIZE = 10

# get cmap and defined nblevels
CMAP   = matplotlib.cm.get_cmap('BuPu', NLEVELS)
        
# ======================================
#
# LOAD H60
#
fname = 'h60_20230129_0500_fdk.nc.gz'
print('reading %s ...'%fname)
try:
    hdle = gzip.open(fname, 'rb')
    ds = xr.open_dataset(hdle,engine='h5netcdf')   
except:
    print('\n\nERROR: cannot read file %s\n'%fname)
    sys.exit(-1)    
print('... done')

# ======================================
#
# get coordinates
# from https://holmdk.github.io/2020/01/19/geostationary_satellite_img.html
#
print('computing coordinates ...')

source_crs = CRS.from_proj4(ds.attrs['gdal_projection'])
crs_dict = source_crs.to_dict()

projection_name = crs_dict['proj']
lon_0 = crs_dict['lon_0']
a     = crs_dict['a']
b     = crs_dict['b']
h     = crs_dict['h']
proj  = crs_dict['proj']

# or swap x and y ?
height = ds.x.size
width  = ds.y.size

# we make an AreaDefinition from the pyresample package
# Raspaud Martin, pytroll, 07/02/2023
# The seviri_0deg area you are loading is describing the area of level 1 seviri data, 
# which is flipped ud and lr by design, with the x and y coordinates being reversed. 
# My suspicion is that the h60 product corrected this to present a product in the upright orientation. 
# Yes, just swap the lower left and upper right corners in the area extent, that should do the trick.
# aex = (aex[2], aex[3], aex[0], aex[1])
# But I see you have the cfac and coff (and probably lfac and loff too) in your file, 
# you could use that to derive the area extent. There is a description in the cgms 03 document I believe, but you can also look here:
# https://github.com/pytroll/satpy/blob/main/satpy/readers/_geos_area.py#L63
#
# maybe there is already a parser somewhere in satpy or pyproj ?
parse_dict = {}
for keyval in ds.attrs['cgms_projection'].split():
    try:
        key,val = keyval.split('=')
        parse_dict[key] = val
    except:
        pass
pdict = {}
pdict['scandir'] = 'N2S'
pdict['h']       = float(parse_dict['+h'])*1000 - float(parse_dict['+r_eq'])*1000 
pdict['loff']    = float(parse_dict['+loff'])
pdict['coff']    = float(parse_dict['+coff'])
pdict['lfac']    = float(parse_dict['+lfac'])
pdict['cfac']    = float(parse_dict['+cfac'])
pdict['ncols']   = ds.x.size
pdict['nlines']  = ds.y.size
# ~ (-5567248.28340708, -5567248.28340708, 5570248.686685662, 5570248.686685662)
area_extent =   get_area_extent(pdict)
# but this is for seviri L1B which is swapped
# so it should be
# (area_extent[2],area_extent[3],area_extent[0],area_extent[1])
# (5570248.686685662, 5570248.686685662, -5567248.28340708, -5567248.28340708)
area_def = AreaDefinition('areaD', projection_name, 'areaD',
                          {'lon_0': lon_0,
                           'a': a,
                           'b': b,
                           'h': h,
                           'proj': proj},
                          height, width,
                          (area_extent[2],area_extent[3],area_extent[0],area_extent[1])
                          )
# get coordinates
AREA_CRS = area_def.to_cartopy_crs()

# ======================================
#
# APPLY FILTER VALUES
#
ds   = ds.where(ds.rr >= RR_MIN)

# ======================================
#
# INIT FIGURE
#
print('plotting ...')
fig = plt.figure(figsize=(20,12))   
ax = fig.add_subplot(1,1,1,projection=PROJ)
# ocean
feature = cfeature.NaturalEarthFeature(category='physical', name='ocean', scale=RESOLUTION,
                                edgecolor='none',   
                                facecolor=cfeature.COLORS['water'],zorder=10)    
ax.add_feature(feature)
# land
feature = cfeature.NaturalEarthFeature(category='physical', name='land', scale=RESOLUTION,
                                edgecolor='none',   
                                facecolor=cfeature.COLORS['land'],zorder=20)    
ax.add_feature(feature)
# coastlines
ax.coastlines(resolution=RESOLUTION,zorder=30)
# set extent
if EXTENT is not None: ax.set_extent(EXTENT, ccrs.PlateCarree())
# grid lines
ax.gridlines(draw_labels=True, dms=True, x_inline=False, y_inline=False,zorder=30)


# ======================================
#
# MAP H60
#
data = ds.rr
map_rr = ax.imshow(data,transform=AREA_CRS,cmap=CMAP,zorder=100)


# ======================================
#
# POST MAP
#

# colorbar
label = 'Rain Rate [mm.hr$^{-1}$]'
plt.colorbar(map_rr, ax=ax, shrink=.8, pad=.05, orientation='vertical',label=label,alpha=1)


# title
ax.set_title(fname)
figname = os.path.splitext(os.path.basename(fname))[0]+'_imshow.png'
print(figname)
plt.savefig(figname,bbox_inches='tight')

