# Dealing with Eumetsat Hydro-SAF H60 precipitation products

Thanks to Martin Raspaud from the [pytroll](http://pytroll.github.io/) group for the support on the manual AreaDefinition
[pytroll](http://pytroll.github.io/) is responsible for the development of [satpy](https://satpy.readthedocs.io/en/stable/) and [pyresample](https://pyresample.readthedocs.io/en/latest/).


## Objective

The objective is to create contour or scatter maps from the H60 products and to clip the original dataset from a geographical extent.

H60 products are delivered in the original geostationary coordinate frame and the corresponding longitude/latitude coordinates are delivered in a separated file.

Note that `imshow` is the most efficient way to display the map and it does not require the corresponding longitude/latitude grid, just the correct CRS.
 
But, the final objective is to collocate the precipitation products with third party missions. It is thus necessary to be able to compute the corresponding (x,y) and (longitude,latitude) grid.

There is an additional difficulty here since the `AreaDefinition` (in the sense of `pyresample`) has to be manually defined in order to correctly display the dataset using the global attributes defining the projection.

- [`map_contourf_global_h60_using_precomputed_latlon.py`](https://gitlab.com/bpicard_fluctus/h60_howto/-/blob/main/map_contourf_global_h60_using_precomputed_latlon.py)
`contourf` mapping based on precomputed longitude/latitude file

- [`map_imshow_global_h60.py`](https://gitlab.com/bpicard_fluctus/h60_howto/-/blob/main/map_imshow_global_h60.py)
The `pyresample.geometry.AreaDefinition` approach is used to the cartopy CRS of the image.
Then `imshow` is used to display the figure

- [`map_contourf_clip_zoom_h60.py`](https://gitlab.com/bpicard_fluctus/h60_howto/-/blob/main/map_contourf_clip_zoom_h60.py)
The `pyresample.geometry.AreaDefinition` approach is used to compute the (x,y) and (longitude,latitude) grid.
Then, the same approach is used to clip the dataset from a geographical longitude/latitude extent.
Then `contourf` is used to map the clipped dataset.

## Required librairies
- load dataset
    - gzip
    - xarray with h5netcdf capability (to load gzip handle files) (see [xarray install guide](https://docs.xarray.dev/en/stable/getting-started-guide/installing.html))
- coordinates / extent / clip
    - [satpy](https://satpy.readthedocs.io/en/stable/)
    - [pyresample](https://pyresample.readthedocs.io/en/latest/)
- matplotlib
- cartopy



## H60 products

H60 precipitation gridded products are described as
_Instantaneous precipitation maps generated combining geostationary (GEO) IR images from operational geostationary satellites 'calibrated' by precipitation measurements from MW images on Low Earth Orbit (LEO) satellites, processed soon after each acquisition of a new image from GEO._


the products are available from Eumetsat Hydro-SAF
- https://hsaf.meteoam.it/

the precipitation products are listed here
- https://hsaf.meteoam.it/Products/ProductsList?type=precipitation

the H60 products are detailed here
- https://hsaf.meteoam.it/Products/Detail?prod=H60B

## Note on longitude / latitude grids

Since H60 products are built from Seviri products, the natural grid is the geostationary grid from MSG4.
In order to lighten the product size, the longitude/latitude grids are available in a separated file
The longitude / latitude grids are available here by FTP:
- ftp://ftphsaf.meteoam.it/products/matlab_code/
also available in this git repository

## Usefull related links to deal with geostationary products
- https://satpy.readthedocs.io/en/stable/
- https://holmdk.github.io/2020/01/19/geostationary_satellite_img.html
- https://goes2go.readthedocs.io/en/latest/index.html
- https://geonetcast.wordpress.com/2019/08/02/plot-0-5-km-goes-r-full-disk-regions/


    

